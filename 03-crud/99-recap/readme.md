# RECAP

At this point, we have:

1. A REST controller, accepting urls and answering with JSON
2. A React app that can communicate with it
3. The basis for animations, loading, and other basic UI users need
4. Routing on the front-end side
5. Authentication
6. file uploads
7. code organization

That's really a lot of technical ground covered. In the next part, we will try to make our application look good.