import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

class App extends Component {
  state = { contacts_list:[] }
  componentDidMount(){
    fetch('//localhost:8080/contacts/list')
      .then( response => response.json() )
      .then( text => this.setState({contacts_list:text}))
  }
  render() {
    const { contacts_list } = this.state
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          { contacts_list.map( contact => 
                <div key={contact.id}>
                  <p>{contact.id} -  {contact.name}</p>
                </div>
            )
          }
          <p>
            Edit <code>src/App.js</code> and save to reload.
          </p>
          <a
            className="App-link"
            href="https://reactjs.org"
            target="_blank"
            rel="noopener noreferrer"
          >
            Learn React
          </a>
        </header>
      </div>
    );
  }
}

export default App;
