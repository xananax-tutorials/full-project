# Choosing a Dress for React

If I was a designer, I'd design my app, but I am not.

I will need to choose a UI library to use with React; There are many.

I will choose [React Native Web](https://github.com/necolas/react-native-web). It's a nice framework, looks good, and makes it kinda easier to export to Android or IOS later.

React Native App provides us with cross platform components, and very basic styles, but won't make our app look good. That's up to us. I will introduce later a library to avoid having to style.  

**Note**: React Native Web *will* makes certain simple things more complex. However, in return, it makes a very complex thing more simple: write code once, and export for all platforms. It can be a worthy trade-off, depending on your situation, so it's worth testing at least.

We would normally have to edit config files to be able to include it, but luckily, `create-react-app` comes ready to handle `react-native-web`, so we have to do very little to include it.

Let's add it. Move into the `front`, and run:

```sh
npm install --save react-native-web react-art
```

Then, we'll follow the (official documentation)[https://github.com/necolas/react-native-web/blob/master/docs/guides/client-side-rendering.md]. Open the file `index.js`, and change the contents to:

```js
import { AppRegistry } from 'react-native';
import App from './App';

AppRegistry.registerComponent('App', () => App);

AppRegistry.runApplication('App', {
  initialProps: {},
  rootTag: document.getElementById('root')
});
```

We also need to change `App.js` to use React-Native components. Edit `App.js` and change the contents to:

```js
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

const styles = StyleSheet.create({
  box: { padding: 10 },
  text: { fontWeight: 'bold' }
});

class App extends React.Component {
  render() {
    return (
      <View style={styles.box}>
        <Text style={styles.text}>Hello, world!</Text>
      </View>
    );
  }
}

export default App
```

### Changes:

- in `index.js`:
  - we have removed `ReactDOM`, since `react-native-web` now takes care of rendering our application
  - we do not import `index.css` anymore, since `react-native-web` is going to take care of styling
  - we do not use the service worker. It's useful and important, but outside the scope of this exercise
  - we have removed the import of React, since it's not used
  - we have changed `ReactDOM.render` to `AppRegistry.registerComponent` and `AppRegistry.runApplication`, as specified in the documentation
- in `App.js`:
  - we have changed the `App.js` to use `react-native-web` components

You will notice we are specifying the component's style in Javascript instead of CSS. This is because `react-native` requires styles to be provided in this manner.

To know what components are available, check the [examples](https://github.com/necolas/react-native-web/tree/master/packages/examples)

If you want more options, you add a [UI Kit](./b-react-native-ui-kit).